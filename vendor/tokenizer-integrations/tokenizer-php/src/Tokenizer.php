<?php

/**
 * PHP Tokenizer Class
 *
 * @author Frank Broersen <frank@amsterdamstandard.com>
 * @copyright Amsterdam Standard
 * @homepage http://amsterdamstandard.com
 */

namespace TokenizerPhp;

use TokenizerPhp\Tokenizer\Exception as Exception,
    TokenizerPhp\Tokenizer\Connector as Connector;
use TokenizerPhp\Tokenizer\Exception\RedirectUrlNotGivenException;
use TokenizerPhp\Tokenizer\Exception\ResponseNotGivenException;

class Tokenizer
{
    /**
     * Connector definition
     * @var \TokenizerPhp\Tokenizer\Connector
     */
    private $connector = null;

    /**
     * Property for definition of session container
     * @var \TokenizerPhp\Tokenizer\Storage\SessionStorageInterface
     */
    private $sessionStorage = null;

    /**
     * Basic Tokenizer configuration
     * @var array
     */
    private $config = array(
        'host'   => 'https://api.tokenizer.com/',
        'create' => 'v1/authentications.json',
        'verify' => 'v1/authentication/{id}.json',
        'check'  => 'v1/application/{app_id}.json?app_key={app_key}',
    );

    /**
     * Constructor
     * @param array $config array that should contain app_id, app_key and host
     * @throws \Exception
     */
    public function __construct($config = array()) {
        foreach ($config as $var => $value) {
            $this->config[$var] = $value;
        }

        $this->connector = new Connector();

        if(!$this->validateConfig()) {
            throw new Exception("Please supply the correct tokenizer config [app_id,app_key,host]");
        }
    }

    /**
     * Validate if we have an app id, key and host
     * @return boolean
     */
    private function validateConfig() {
        foreach (array('app_id', 'app_key', 'host', 'create', 'verify') as $var) {
            if (!isset($this->config[$var]) || trim($this->config[$var]) == '') {
                return false;
            }
        }
        return true;
    }

    /**
     * Create a tokenizer authentication
     * @param string $email The email address of the user you want to login
     * @param string $returnUrl The url of the script that checks the token
     * @param bool $redirect Auto redirect the user to the waiting page
     */
    public function createAuth($email, $returnUrl, $redirect = true)
    {
        $this->connector->setHost($this->config['host']);
        $this->connector->setPath($this->config['create']);

        if($returnUrl === null) {
            throw new RedirectUrlNotGivenException("Return url not given");
        }

        // Create authentication
        $response = $this->connector->curlPost('create', [
            'app_id'    => $this->config['app_id'],
            'app_key'   => $this->config['app_key'],
            'usr_email' => $email,
        ]);

        if($response === null) {
            throw new ResponseNotGivenException("Unable to reach remote service");
        }

        // Store returned id
        $this->getSessionStorage()->set('tokenizer_id', $response->id);
                
        // Build waiting page url
        $uri = $response->wait_url . '?redirect=' . $returnUrl . $response->id;
                
        // Redirect
        if($redirect === true) {
            header("Location: $uri");
            exit;
        }
        
        return $redirect;    
    }    
    
    /**
     * Verify if the id of a given token has been confirmed by the user
     * @param int $id
     */
    public function verifyAuth($id)
    {
        $this->connector->setHost($this->config['host']);
        $this->connector->setPath($this->config['verify'], ['{id}' => $id]);
        
        // Verify authentication
        $response = $this->connector->curlGet('verify', [
            'app_id'    => $this->config['app_id'],
            'app_key'   => $this->config['app_key'],
        ]);
        
        if($response->state == 'accepted') {
            return true;
        }
        return false;
    }   
          
    /**
     * Verify that the application details are working
     */
    public function verifyConfig()
    {
        $this->connector->setHost($this->config['host']);
        $this->connector->setPath($this->config['check'], [
            '{app_id}'  => $this->config['app_id'],
            '{app_key}' => $this->config['app_key'],
        ]);

        return $this->connector->curlGet('config');
    }

    /**
     * Set session storage
     * @param \TokenizerPhp\Tokenizer\Connector $scope
     */
    public function setConnector(Connector $connector)
    {
        $this->connector = $connector;
    }

    /**
     * Set session storage
     * @param \TokenizerPhp\Tokenizer\Storage\SessionStorageInterface $scope
     */
    public function setSessionStorage(\TokenizerPhp\Tokenizer\Storage\SessionStorageInterface $scope)
    {
        $this->sessionStorage = $scope;
    }

    /**
     * Get session storage. Default: GlobalSessionStorage
     * @return \TokenizerPhp\Tokenizer\Storage\SessionStorageInterface
     */
    public function getSessionStorage()
    {
        return ($this->sessionStorage !== null ? $this->sessionStorage : new \TokenizerPhp\Tokenizer\Storage\GlobalSessionStorage);
    }


}
