<?php
/**
 * Exception Class
 *
 * @author Mateusz Aniolek <mateusz.aniolek@amsterdam-standard.pl>
 * @copyright Amsterdam Standard
 */

namespace TokenizerPhp\Tokenizer\Exception;

class RedirectUrlNotGivenException extends \TokenizerPhp\Tokenizer\Exception {
    
}

